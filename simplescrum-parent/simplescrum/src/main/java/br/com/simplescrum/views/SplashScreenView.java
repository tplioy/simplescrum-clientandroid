package br.com.simplescrum.views;

import android.content.Intent;
import android.os.Bundle;
import br.com.droidfast.views.AbstractSplashView;
import br.com.simplescrum.R;

public class SplashScreenView extends AbstractSplashView {

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		 setScreenToLoadAfterSplash(new Intent("SS_GO_PROJECTS"));
		 setSplashDurationTime(1000);
		 initSplashView(R.layout.splash_screen);
	}

}
