package br.com.simplescrum.modelo;

import java.util.List;

public class ProjectsResponse {

	private List<Project> projects;
	
	public ProjectsResponse() {
		// TODO Auto-generated constructor stub
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}
	
}
