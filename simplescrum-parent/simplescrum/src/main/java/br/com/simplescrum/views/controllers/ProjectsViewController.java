package br.com.simplescrum.views.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;

import android.os.AsyncTask;
import br.com.droidfast.controllers.AbstractViewController;
import br.com.simplescrum.modelo.Project;
import br.com.simplescrum.modelo.ProjectsResponse;
import br.com.simplescrum.rest.controller.SimpleScrumRestClient;
import br.com.simplescrum.views.ProjectsView;

public class ProjectsViewController extends
		AbstractViewController<ProjectsView> {

	private ProjectsResponse projectsResponse;

	public ProjectsViewController(ProjectsView activity) {
		super(activity);
		// TODO Auto-generated constructor stub
	}

	public void downloadProjects() {
		if (activity.isNetWorkAvailable()) {
			activity.startDialog("Loading ..", "Baixando Projetos");
			new DownloadProjectsAsyncTask().execute();
		} else {
			activity.sendToastMsg("Por favor, verifique sua conex�o com a internet!");
		}

	}

	private class DownloadProjectsAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			projectsResponse = SimpleScrumRestClient.getInstance()
					.getProjects();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if (SimpleScrumRestClient.getInstance().getResponseStatusCode()
					.equals(HttpStatus.OK)) {
				activity.handleSuccessufulDownloadProjects();
				activity.finishDialog();
			} else {
				activity.sendToastMsg("Um erro ocorreu!");
				activity.finishDialog();
				activity.finish();
			}
			activity.finishDialog();
		}

	}
	
	public List<Project> getProjects(){
		return projectsResponse.getProjects();
	}

}
