package br.com.simplescrum.util;

import android.net.Uri;

import com.google.gson.Gson;

public class Parser {

	public static Object parseJsonToObj(String json, Class<?> objClass) {
		Gson gson = new Gson();
		return gson.fromJson(json.replace("\"", "'"), objClass);
	}

	public static String escapeJson(String json) {
		return Uri.encode(json);
	}
}
