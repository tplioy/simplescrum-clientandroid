package br.com.simplescrum.views;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.ListView;
import br.com.droidfast.views.AbstractView;
import br.com.simplescrum.R;
import br.com.simplescrum.adapters.BacklogItemtAdapter;
import br.com.simplescrum.adapters.ProjectAdapter;
import br.com.simplescrum.views.controllers.BacklogViewController;
import br.com.simplescrum.views.controllers.ProjectsViewController;

public class BacklogView extends AbstractView {

	private LinearLayout lv_backlog_llay;
	private ListView lv_backlog;
	private BacklogViewController controller;
	private BacklogItemtAdapter backlogItemAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initView(R.layout.backlog);
		fillList();
	}

	@Override
	public void initUIComponents() {
		lv_backlog = (ListView) findViewById(R.id.backlog_view_lv_backlog);
		lv_backlog_llay = (LinearLayout) findViewById(R.id.projects_view_list_llay);
	}

	@Override
	public void initUIEvents() {
		// TODO Auto-generated method stub

	}

	@Override
	public void initViewControllers() {
		controller = new BacklogViewController(this);
	}

	private void fillList() {
		controller.downloadBacklogItems();
	}

	public void handleSuccessufulDownloadProjects() {
		backlogItemAdapter = new BacklogItemtAdapter(this,
				controller.getBacklogItems());
		lv_backlog.setAdapter(backlogItemAdapter);
		lv_backlog_llay.removeAllViews();
		lv_backlog_llay.addView(lv_backlog);
	}

}
