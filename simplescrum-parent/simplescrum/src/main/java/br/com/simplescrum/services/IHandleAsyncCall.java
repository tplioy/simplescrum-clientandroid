package br.com.simplescrum.services;

public interface IHandleAsyncCall {

	public void onSuccess();
	public void onFail();
	public void onExpectedFail();
	
}
