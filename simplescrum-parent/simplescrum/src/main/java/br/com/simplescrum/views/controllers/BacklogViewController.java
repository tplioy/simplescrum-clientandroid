package br.com.simplescrum.views.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;

import android.content.IntentSender.SendIntentException;
import android.os.AsyncTask;
import br.com.droidfast.controllers.AbstractViewController;
import br.com.simplescrum.modelo.Backlog;
import br.com.simplescrum.modelo.BacklogItem;
import br.com.simplescrum.modelo.Project;
import br.com.simplescrum.modelo.ProjectsResponse;
import br.com.simplescrum.rest.controller.SimpleScrumRestClient;
import br.com.simplescrum.util.Constantes;
import br.com.simplescrum.views.BacklogView;
import br.com.simplescrum.views.ProjectsView;

public class BacklogViewController extends AbstractViewController<BacklogView> {

	private Backlog backlogResponse;
	private String projectId;

	public BacklogViewController(BacklogView activity) {
		super(activity);
		getFromIntent();
	}

	private void getFromIntent() {
		projectId = activity.getIntent().getExtras()
				.getString(Constantes.BUNDLE_PROJECT_ID);
	}

	public void downloadBacklogItems() {
		if (activity.isNetWorkAvailable()) {
			activity.startDialog("Loading ..", "Baixando Projetos");
			new DownloadBacklogItemsAsyncTask().execute();
		} else {
			activity.sendToastMsg("Por favor, verifique sua conex�o com a internet!");
		}

	}

	private class DownloadBacklogItemsAsyncTask extends
			AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			backlogResponse = SimpleScrumRestClient.getInstance()
					.getBacklogItems(projectId);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if (SimpleScrumRestClient.getInstance().getResponseStatusCode()
					.equals(HttpStatus.OK)) {
				if (!backlogResponse.getItems().isEmpty()) {
					activity.handleSuccessufulDownloadProjects();
					activity.finishDialog();
				} else {
					activity.sendToastMsg("N�o existem items no backlog!");
					activity.finishDialog();
					activity.finish();
				}
			} else {
				activity.sendToastMsg("Um erro ocorreu!");
				activity.finishDialog();
				activity.finish();
			}
			activity.finishDialog();
		}

	}

	public List<BacklogItem> getBacklogItems() {
		return backlogResponse.getItems();
	}

}
