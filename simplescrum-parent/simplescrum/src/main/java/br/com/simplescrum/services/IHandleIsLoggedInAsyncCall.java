package br.com.simplescrum.services;

public interface IHandleIsLoggedInAsyncCall {

	public void loggedIn();
	public void notLoggedIn();
}
