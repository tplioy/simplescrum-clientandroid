package br.com.simplescrum.rest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import br.com.droidfast.util.LogUtil;
import br.com.simplescrum.modelo.Backlog;
import br.com.simplescrum.modelo.CustomerLoginRequest;
import br.com.simplescrum.modelo.ProjectsResponse;
import br.com.simplescrum.util.Constantes;

public class ProjectsRestClient {

	private static final String PROJECTS_URL = Constantes.APPLICATION_HOST
			+ "/projects";
	private static final String BACKLOG_ITEMS_URL = Constantes.APPLICATION_HOST
			+ "/projects/%s/backlog/items/";
	private RestTemplateExt rest;

	public ProjectsRestClient(RestTemplateExt rest) {
		this.rest = rest;
	}

	public ProjectsResponse getProjects() {
		LogUtil.getInstance().logJsonMsg(
				"ProjectsRestClient :: getProjects :: request :: url",
				PROJECTS_URL);

		ProjectsResponse response = null;
		try {
			response = rest.getForObject(PROJECTS_URL, ProjectsResponse.class);
			SimpleScrumRestClient.getInstance().setResponseStatusCode(
					HttpStatus.OK);
		} catch (HttpClientErrorException e) {
			SimpleScrumRestClient.getInstance().setResponseStatusCode(
					e.getStatusCode());
		} catch (Exception e) {
			SimpleScrumRestClient.getInstance().setResponseStatusCode(
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

		LogUtil.getInstance().logJsonMsg(
				"ProjectsRestClient :: getProjects :: response :: statusCode",
				SimpleScrumRestClient.getInstance().getResponseStatusCode());
		LogUtil.getInstance().logJsonMsg(
				"ProjectsRestClient :: getProjects :: response :", response);

		return response;
	}

	public Backlog getBacklogItems(String projectId) {

		String url = String.format(BACKLOG_ITEMS_URL, projectId);
		LogUtil.getInstance().logJsonMsg(
				"ProjectsRestClient :: getBacklogItems :: request :: url", url);

		Backlog response = null;
		try {
			response = rest.getForObject(url, Backlog.class);
			SimpleScrumRestClient.getInstance().setResponseStatusCode(
					HttpStatus.OK);
		} catch (HttpClientErrorException e) {
			SimpleScrumRestClient.getInstance().setResponseStatusCode(
					e.getStatusCode());
		} catch (Exception e) {
			SimpleScrumRestClient.getInstance().setResponseStatusCode(
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

		LogUtil.getInstance()
				.logJsonMsg(
						"ProjectsRestClient :: getBacklogItems :: response :: statusCode",
						SimpleScrumRestClient.getInstance()
								.getResponseStatusCode());
		LogUtil.getInstance()
				.logJsonMsg(
						"ProjectsRestClient :: getBacklogItems :: response :",
						response);

		return response;
	}

}
