package br.com.simplescrum.util;

import android.content.Context;

public class UserPreferences extends PreferencesController {

	public UserPreferences(Context ctx) {
		preferences = ctx.getSharedPreferences(Constantes.USER_PREFERENCES,
				Context.MODE_PRIVATE);
		editor = preferences.edit();
	}

	public void clearPreferences() {
		editor.clear();
		editor.commit();
	}

	public boolean isPreferencesEmpty() {
		return preferences.getAll().isEmpty();
	}

	public void storeUserToken(String userToken) {
		editor.putString("userToken", userToken);
		editor.commit();
	}

	public String getUserToken() {
		return preferences.getString("userToken", null);
	}

}
