package br.com.simplescrum.modelo;

import java.util.List;

public class Backlog {

	private List<BacklogItem> items;
	
	public Backlog() {
		// TODO Auto-generated constructor stub
	}

	public List<BacklogItem> getItems() {
		return items;
	}

	public void setItems(List<BacklogItem> items) {
		this.items = items;
	}
}
