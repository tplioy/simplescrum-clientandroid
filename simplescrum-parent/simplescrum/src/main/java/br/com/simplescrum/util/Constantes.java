package br.com.simplescrum.util;

public class Constantes {
	public static final String GA_TRACK_MSG = "/Android/";


	private static final String APPLICATION_HOST_HEROKU = "http://cloudretail.herokuapp.com/api/v1/";
	private static final String MY_MACHINE_HOST = "http://10.0.1.2:3000";

	public static final String APPLICATION_HOST = MY_MACHINE_HOST;


	public static final String USER_PREFERENCES = "userPreferences";


	public static final String BUNDLE_PROJECT_ID = "projectId";

}
