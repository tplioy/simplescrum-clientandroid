package br.com.simplescrum.util;


import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public abstract class PreferencesController {

	protected SharedPreferences preferences;
	protected Editor editor;
}
