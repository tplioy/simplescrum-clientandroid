package br.com.simplescrum.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.simplescrum.R;
import br.com.simplescrum.modelo.Project;

public class ProjectAdapter extends BaseAdapter {

	private Context ctx;
	private List<Project> projects;
	private LayoutInflater inflater;

	public ProjectAdapter(Context ctx, List<Project> lista) {
		this.ctx = ctx;
		this.projects = lista;
	}

	@Override
	public int getCount() {

		return projects.size();
	}

	@Override
	public Object getItem(int position) {

		return projects.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	static class ViewHolder {
		TextView tv_name;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			inflater = (LayoutInflater) ctx
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.project_row, null);

			holder = new ViewHolder();
			holder.tv_name = (TextView) convertView
					.findViewById(R.id.project_row_tv_name);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		Project p = projects.get(position);
		holder.tv_name.setText(p.getName());

		return convertView;
	}

}