package br.com.simplescrum.modelo;

public class CustomerLoginRequest {

	private CustomerLogin customer;
	
	public CustomerLoginRequest() {
		// TODO Auto-generated constructor stub
	}
	
	public CustomerLoginRequest(String login , String password) {
		customer = new CustomerLogin();
		customer.setLogin(login);
		customer.setPassword(password);
	}
	
	
	public CustomerLogin getCustomer() {
		return customer;
	}




	public void setCustomer(CustomerLogin customer) {
		this.customer = customer;
	}




	private class CustomerLogin {
		private String login;
		private String password;
		
		public CustomerLogin() {
			// TODO Auto-generated constructor stub
		}

		public String getLogin() {
			return login;
		}

		public void setLogin(String login) {
			this.login = login;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}
		
	}
}


