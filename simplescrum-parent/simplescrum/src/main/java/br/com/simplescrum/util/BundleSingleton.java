package br.com.simplescrum.util;

import android.os.Bundle;


public class BundleSingleton {
	private static BundleSingleton instance;
	private Bundle bundle;
	
	private BundleSingleton() {
		bundle = new Bundle();
	}
	
	public static Bundle getBundle(){
		if(instance==null)
			instance = new BundleSingleton();
		return instance.bundle;
	}
}
