package br.com.simplescrum.rest.controller;

import org.springframework.http.HttpStatus;

import br.com.droidfast.util.LogUtil;
import br.com.simplescrum.modelo.Backlog;
import br.com.simplescrum.modelo.CustomerLoginRequest;
import br.com.simplescrum.modelo.ProjectsResponse;

public class SimpleScrumRestClient {

	private static SimpleScrumRestClient instance;

	private RestTemplateExt rest;
	private ProjectsRestClient projectsClient;
	private HttpStatus responseStatusCode;

	private SimpleScrumRestClient() {
		rest = new RestTemplateExt();
	}

	public static SimpleScrumRestClient getInstance() {
		if (instance == null) {
			synchronized (SimpleScrumRestClient.class) {
				instance = new SimpleScrumRestClient();
			}
		}
		return instance;
	}

	private ProjectsRestClient getProjectsRestClientInstance() {
		if (projectsClient == null) {
			synchronized (SimpleScrumRestClient.class) {
				projectsClient = new ProjectsRestClient(rest);
			}
		}
		return projectsClient;
	}

	public ProjectsResponse getProjects() {
		return getProjectsRestClientInstance().getProjects();
	}

	public Backlog getBacklogItems(String projectId) {
		return getProjectsRestClientInstance().getBacklogItems(projectId);
	}

	public HttpStatus getResponseStatusCode() {
		return responseStatusCode;
	}

	void setResponseStatusCode(HttpStatus responseStatusCode) {
		this.responseStatusCode = responseStatusCode;
	}

	public static void reset() {
		instance = null;
	}

	public void storeToken(String token) {
		LogUtil.getInstance().logMsg("storeToken :: Token:", token);
		rest.getRequestHttpHeaders().remove("AuthToken");
		rest.getRequestHttpHeaders().add("AuthToken", token);
	}

}
