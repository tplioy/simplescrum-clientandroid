package br.com.simplescrum.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import br.com.droidfast.views.AbstractView;
import br.com.simplescrum.R;
import br.com.simplescrum.adapters.ProjectAdapter;
import br.com.simplescrum.modelo.Project;
import br.com.simplescrum.util.Constantes;
import br.com.simplescrum.views.controllers.ProjectsViewController;

public class ProjectsView extends AbstractView {

	private LinearLayout lv_projects_llay;
	private ListView lv_projects;
	private ProjectsViewController controller;
	private ProjectAdapter projectAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initView(R.layout.projects);
		fillList();
	}

	@Override
	public void initUIComponents() {
		lv_projects = (ListView) findViewById(R.id.projects_view_lv_projects);
		lv_projects_llay = (LinearLayout) findViewById(R.id.projects_view_list_llay);
	}

	@Override
	public void initUIEvents() {
		lv_projects.setOnItemClickListener(new ProjectListOnItemClick());
	}

	@Override
	public void initViewControllers() {
		controller = new ProjectsViewController(this);
	}

	private void fillList() {
		controller.downloadProjects();
	}

	public void handleSuccessufulDownloadProjects() {
		projectAdapter = new ProjectAdapter(this, controller.getProjects());
		lv_projects.setAdapter(projectAdapter);
		lv_projects_llay.removeAllViews();
		lv_projects_llay.addView(lv_projects);
	}

	private class ProjectListOnItemClick implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long id) {
			String selectedId = ((Project) lv_projects.getAdapter().getItem(
					position)).getId();
			startBacklogActivityForProject(selectedId);
		}
	}

	private void startBacklogActivityForProject(String id) {
		Intent i = new Intent("SS_GO_BACKLOG");
		i.putExtra(Constantes.BUNDLE_PROJECT_ID, id);
		startActivity(i);
	}

}
