package br.com.simplescrum.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.simplescrum.R;
import br.com.simplescrum.modelo.BacklogItem;
import br.com.simplescrum.modelo.Project;

public class BacklogItemtAdapter extends BaseAdapter {

	private Context ctx;
	private List<BacklogItem> items;
	private LayoutInflater inflater;

	public BacklogItemtAdapter(Context ctx, List<BacklogItem> lista) {
		this.ctx = ctx;
		this.items = lista;
	}

	@Override
	public int getCount() {

		return items.size();
	}

	@Override
	public Object getItem(int position) {

		return items.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	static class ViewHolder {
		TextView tv_name;
		TextView tv_estimate;
		

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			inflater = (LayoutInflater) ctx
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.backlog_item_row, null);

			holder = new ViewHolder();
			holder.tv_name = (TextView) convertView
					.findViewById(R.id.backlog_item_row_tv_name);
			holder.tv_estimate = (TextView) convertView
					.findViewById(R.id.backlog_item_row_tv_estimate);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		BacklogItem b = items.get(position);
		holder.tv_name.setText(b.getName());
		holder.tv_estimate.setText(b.getEstimate());

		return convertView;
	}

}