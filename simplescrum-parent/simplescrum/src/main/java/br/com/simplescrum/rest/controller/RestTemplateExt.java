package br.com.simplescrum.rest.controller;

import java.io.IOException;
import java.net.URI;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

public class RestTemplateExt extends RestTemplate {

	private HttpHeaders headers = new HttpHeaders();

	public HttpHeaders getRequestHttpHeaders() {
		return headers;
	}

	@Override
	protected <t> t doExecute(URI url, HttpMethod method,
			RequestCallback requestCallback,
			ResponseExtractor<t> responseExtractor) throws RestClientException {

		RequestCallbackDecorator requestCallbackDecorator = new RequestCallbackDecorator(
				requestCallback);
		
		return super.doExecute(url, method, requestCallbackDecorator,
				responseExtractor);
	}

	private class RequestCallbackDecorator implements RequestCallback {

		public RequestCallbackDecorator(RequestCallback targetRequestCallback) {
			this.targetRequestCallback = targetRequestCallback;
		}

		private RequestCallback targetRequestCallback;

		public void doWithRequest(ClientHttpRequest request) throws IOException {

			if (headers != null)
				request.getHeaders().putAll(headers);
			

			if (null != targetRequestCallback) {
				targetRequestCallback.doWithRequest(request);
			}
		}
	}

}